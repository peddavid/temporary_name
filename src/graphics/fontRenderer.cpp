namespace graphics {

class TextRenderer {
    private:
    const std::string fontVertexShader =
    R"(#version 330 core
    layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
    out vec2 TexCoords;

    uniform mat4 projection;

    void main()
    {
        gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);
        TexCoords = vertex.zw;
    }
    )";

    const std::string fontFragmentShader =
    R"(#version 330 core
    in vec2 TexCoords;
    out vec4 color;

    uniform sampler2D text;
    uniform vec3 textColor;

    void main()
    {    
        vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);
        color = vec4(textColor, 1.0) * sampled;
    }
    )";

    struct Character {
        std::unique_ptr<graphics::Texture> texture;  // ID handle of the glyph texture
        glm::ivec2 Size;       // Size of glyph
        glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
        FT_Pos Advance;    // Offset to advance to next glyph
    };


    std::array<Character, 128> characters;

    const VAO fontVao{};
    const VBO fontVbo = VBO::dynamic<float, 6>(0, 4);

    ShaderProgram fontShader{
        Shader { GL_VERTEX_SHADER, fontVertexShader },
        Shader { GL_FRAGMENT_SHADER, fontFragmentShader }
    };

    public:
    TextRenderer() {
        fontShader.enable();
        glm::mat4 projection = glm::ortho(0.0f, 640.0f, 0.0f, 480.0f);
        fontShader.uniformMatrix4fv(fontShader.getUniformLocation("projection"), 1, GL_FALSE, &projection[0][0]);

        fonts::FontLibrary fl;
        auto face = fl.createFace("res/fonts/consola.ttf", 48);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction
        for (GLubyte c = 0; c < 128; c++) {
            auto glyph = face.getChar(c).glyph;
            characters[c] = Character {
                std::make_unique<Texture>(glyph),
                glm::ivec2(glyph->bitmap.width, glyph->bitmap.rows),
                glm::ivec2(glyph->bitmap_left, glyph->bitmap_top),
                glyph->advance.x
            };
        }
    }

    void render(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color) const {
        // Activate corresponding render state	
        fontShader.enable();
        fontShader.uniform3f(fontShader.getUniformLocation("textColor"), color.x, color.y, color.z);
        glActiveTexture(GL_TEXTURE0);
        fontVao.bind();

        for (const auto c : text) {
            const Character* ch = &characters[c];

            const GLfloat xpos = x + ch->Bearing.x * scale;
            const GLfloat ypos = y - (ch->Size.y - ch->Bearing.y) * scale;

            const GLfloat w = ch->Size.x * scale;
            const GLfloat h = ch->Size.y * scale;
            // Update VBO for each character
            const GLfloat vertices[6][4] = {
                { xpos,     ypos + h,   0.0, 0.0 },            
                { xpos,     ypos,       0.0, 1.0 },
                { xpos + w, ypos,       1.0, 1.0 },

                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos + w, ypos,       1.0, 1.0 },
                { xpos + w, ypos + h,   1.0, 0.0 }           
            };
            ch->texture->bind();
            fontVbo.subData(0, sizeof(vertices), vertices);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
            x += (ch->Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
        }
    }
};

}