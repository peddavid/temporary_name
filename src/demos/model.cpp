namespace graphics {
struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
};

template<>
struct base_type<Vertex> {
    using value = float;
};

struct ModelTexture {
    GLuint id;
    std::string type;
    aiString path;
};

class Mesh {
public:
    std::vector<ModelTexture> textures;

    Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<ModelTexture> textures) :
        textures{ textures }, vbo{ vertices, 3, 3, 2 }, ebo{ indices }, elementsCount { static_cast<GLsizei>(indices.size()) } {
    }

    void draw(const ShaderProgram& shader) {
        GLuint diffuseNr = 1;
        GLuint specularNr = 1;
        for (GLuint i = 0; i < this->textures.size(); i++)
        {
            glActiveTexture(GL_TEXTURE0 + i);

            std::string name = this->textures[i].type;
            std::string number = (name == "texture_diffuse") ? std::to_string(diffuseNr++) : std::to_string(specularNr++);

            glUniform1i(shader.getUniformLocation(("material." + name + number).c_str()), i);
            glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
        }
        glActiveTexture(GL_TEXTURE0);

        // Draw mesh
        vao.bind();
        glDrawElements(GL_TRIANGLES, elementsCount, GL_UNSIGNED_INT, 0);
        vao.unbind();
    }

private:
    const graphics::VAO vao{};
    const graphics::EBO ebo;
    const graphics::VBO vbo;
    const GLsizei elementsCount;
};

GLint textureFromFile(const char* path, std::string directory);

class Model
{
public:
    /*  Functions   */
    // Constructor, expects a filepath to a 3D model.
    Model(GLchar* path)
    {
        this->loadModel(path);
    }

    // Draws the model, and thus all its meshes
    void draw(const ShaderProgram& shader)
    {
        for (GLuint i = 0; i < this->meshes.size(); i++)
            this->meshes[i]->draw(shader);
    }

private:
    /*  Model Data  */
    std::vector<std::unique_ptr<Mesh>> meshes;
    std::string directory;
    std::vector<ModelTexture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.

    // Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    void loadModel(std::string path) {
        // Read file via ASSIMP
        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals);
        // Check for errors
        if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
        {
            std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
            return;
        }
        // Retrieve the directory path of the filepath
        this->directory = path.substr(0, path.find_last_of('/'));

        // Process ASSIMP's root node recursively
        this->processNode(scene->mRootNode, scene);
    }

    // Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void processNode(aiNode* node, const aiScene* scene) {
        // Process each mesh located at the current node
        for (GLuint i = 0; i < node->mNumMeshes; i++) {
            // The node object only contains indices to index the actual objects in the scene.
            // The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
            // this->meshes.push_back(this->processMesh(mesh, scene));
            this->processMesh(mesh, scene);
        }
        // After we've processed all of the meshes (if any) we then recursively process each of the children nodes
        for (GLuint i = 0; i < node->mNumChildren; i++) {
            this->processNode(node->mChildren[i], scene);
        }
    }

    void processMesh(aiMesh* mesh, const aiScene* scene) {
        // Data to fill
        std::vector<Vertex> vertices;
        std::vector<GLuint> indices;
        std::vector<ModelTexture> textures;

        // Walk through each of the mesh's vertices
        for (GLuint i = 0; i < mesh->mNumVertices; i++)
        {
            Vertex vertex;
            glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
            // Positions
            vector.x = mesh->mVertices[i].x;
            vector.y = mesh->mVertices[i].y;
            vector.z = mesh->mVertices[i].z;
            vertex.position = vector;
            // Normals
            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.normal = vector;
            // Texture Coordinates
            if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
            {
                glm::vec2 vec;
                // A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
                // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
                vec.x = mesh->mTextureCoords[0][i].x;
                vec.y = mesh->mTextureCoords[0][i].y;
                vertex.texCoords = vec;
            }
            else
                vertex.texCoords = glm::vec2(0.0f, 0.0f);
            vertices.push_back(vertex);
        }
        // Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
        for (GLuint i = 0; i < mesh->mNumFaces; i++)
        {
            aiFace face = mesh->mFaces[i];
            // Retrieve all indices of the face and store them in the indices vector
            for (GLuint j = 0; j < face.mNumIndices; j++)
                indices.push_back(face.mIndices[j]);
        }
        // Process materials
        if (mesh->mMaterialIndex >= 0)
        {
            aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
            // We assume a convention for sampler names in the shaders. Each diffuse texture should be named
            // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
            // Same applies to other texture as the following list summarizes:
            // Diffuse: texture_diffuseN
            // Specular: texture_specularN
            // Normal: texture_normalN

            // 1. Diffuse maps
            std::vector<ModelTexture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
            textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
            // 2. Specular maps
            std::vector<ModelTexture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
            textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
        }

        // Return a mesh object created from the extracted mesh data
        this->meshes.emplace_back(std::make_unique<Mesh>(vertices, indices, textures));
        // return Mesh(vertices, indices, textures);
    }

    // Checks all material textures of a given type and loads the textures if they're not loaded yet.
    // The required info is returned as a Texture struct.
    std::vector<ModelTexture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName) {
        std::vector<ModelTexture> textures;
        for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
        {
            aiString str;
            mat->GetTexture(type, i, &str);
            // Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
            GLboolean skip = false;
            for (GLuint j = 0; j < textures_loaded.size(); j++)
            {
                if (std::strcmp(textures_loaded[j].path.C_Str(), str.C_Str()) == 0)
                {
                    textures.push_back(textures_loaded[j]);
                    skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
                    break;
                }
            }
            if (!skip)
            {   // If texture hasn't been loaded already, load it
                ModelTexture texture;

                texture.id = textureFromFile(str.C_Str(), this->directory);
                texture.type = typeName;
                texture.path = str;
                textures.push_back(texture);
                this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
            }
        }
        return textures;
    }
};

GLint textureFromFile(const char* path, std::string directory) {
    //Generate texture ID and load texture data
    std::string filename = std::string(path);
    filename = directory + '/' + filename;
    GLuint textureID;
    glGenTextures(1, &textureID);
    int width, height, nrChannels;
    stbi_uc* image = stbi_load(filename.c_str(), &width, &height, &nrChannels, 0);
    // Assign texture to ID
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(image);
    return textureID;
}

}

namespace demos {

const std::string vertexShader =
R"(#version 430 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;

uniform vec3 cameraPos;
uniform mat4 model;
uniform mat4 mvp;

void main()
{
    gl_Position = mvp * vec4(position, 1.0f);

	//diffuse
	FragPos = vec3(model * vec4(position, 1.0));
	Normal = normal;
    TexCoords = texCoords;
}
)";

const std::string fragmentShader =
R"(#version 430 core

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec3 toLightVector;
in vec3 toCameraVector;

out vec4 color;

uniform vec3 lightPos;
uniform vec3 lightColor;

uniform mat4 view;

uniform sampler2D texture_diffuse1;

void main()
{
    //color = vec4(vColor, 1.0f);

    vec3 normal = normalize(Normal);
    vec3 unitLightVector = normalize(toLightVector);
    vec3 unitCameraVector = normalize(toCameraVector);

    vec3 toLightVector = lightPos - FragPos;
    vec3 toCameraVector = (inverse(view) * vec4(0.0, 0.0, 0.0, 1.0)).xyz - FragPos;

    vec3 lightDirection = -unitLightVector;
    vec3 reflectedLightDirection = reflect(lightDirection, Normal);

    float shineDamper = 10.0;
    float reflectivity = 1;
    float specFac = pow(max(dot(reflectedLightDirection, unitCameraVector), 0.0), shineDamper);
    vec3 spec = lightColor * reflectivity * specFac;

    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    color = vec4(diffuse, 1.0) * texture(texture_diffuse1, TexCoords) + vec4(spec, 1.0);
}
)";

class Model : public Game {
    static inline const std::string name = "Model Demo";
    private:
    const graphics::ShaderProgram shader {
        graphics::Shader { GL_VERTEX_SHADER, vertexShader },
        graphics::Shader { GL_FRAGMENT_SHADER, fragmentShader }
    };
    graphics::Model model { "res/nanosuit/nanosuit.obj" };

    GLuint modelLocation = shader.getUniformLocation("model");
    GLuint mvpLocation = shader.getUniformLocation("mvp");

    GLuint lightPosLocation = shader.getUniformLocation("lightPos");
    GLuint lightColorLocation = shader.getUniformLocation("lightColor");
    glm::vec3 lightPos{ -1.0f, 2.0f, -4.0f };

    glm::mat4 projection = glm::perspective(45.0f, 1280.0f / 720.0f, 0.1f, 100.0f);
    glm::vec3 cameraPos{ 0.0f, 0.0f, -16.0f };
    glm::mat4 view = glm::translate(glm::mat4(1.0f), cameraPos);
    glm::vec3 rotation{ 0.0f };

    public:
    Model() {
        shader.enable();
        glUniform3f(lightColorLocation, 1.0f, 1.0f, 1.0f);
        glUniform3f(shader.getUniformLocation("cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);
        glUniformMatrix4fv(shader.getUniformLocation("view"), 1, false, glm::value_ptr(view));

        glEnable(GL_DEPTH_TEST);
    }

    void onUpdate() override {
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 modelV = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -7.0f, 0.0f));
        modelV = glm::rotate(modelV, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
        modelV = glm::rotate(modelV, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
        modelV = glm::rotate(modelV, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
        modelV = glm::scale(modelV, glm::vec3(1.0f));

        glm::mat4 mvp = projection * view * modelV;

        shader.enable();
        glUniform3fv(lightPosLocation, 1, glm::value_ptr(lightPos));
        glUniformMatrix4fv(modelLocation, 1, false, glm::value_ptr(modelV));
        glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, glm::value_ptr(mvp));

        rotation.y += 0.001f;

        model.draw(shader);
    }
};
}
