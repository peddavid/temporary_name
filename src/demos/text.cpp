namespace demos {

class Text : public Game {
    static inline const std::string name = "Text Demo";
    private:
    const graphics::TextRenderer textRenderer = graphics::TextRenderer();
    public:
    void onUpdate() override {
        textRenderer.render("This is sample text", 25.f, 25.f, 1.f, glm::vec3(1.f, 0.0f, 1.f));
        textRenderer.render("Also Simple", 300.f, 400.f, 1.f, glm::vec3(0.3f, 0.f, 1.f));
    }
};

}