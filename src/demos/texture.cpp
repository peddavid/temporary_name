namespace demos {

class Texture : public Game {
    static inline const std::string name = "Texture Demo";
    private:
    inline static const std::string vertexShaderSource =
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

void main() {
    gl_Position = vec4(aPos, 1.0);
    TexCoord = aTexCoord;
}
)";

    inline static const std::string fragmentShaderSource =
R"(#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D ourTexture;

void main() {
    FragColor = texture(ourTexture, TexCoord);
}
)";

    inline static const std::array vertices {
        0.5f,  0.5f, 0.0f, 1.0f, 1.0f,
        0.5f,  -0.5f, 0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f,
        -0.5f, 0.5f, 0.0f, 0.0f, 1.0f
    };

    inline static const std::array indices {
        0, 1, 3,
        1, 2, 3
    };

    const graphics::ShaderProgram shader {
        graphics::Shader { GL_VERTEX_SHADER, vertexShaderSource },
        graphics::Shader { GL_FRAGMENT_SHADER, fragmentShaderSource }
    };
    const graphics::VAO vao;
    const graphics::VBO vbo{vertices, 3, 2};
    const graphics::EBO ebo{indices};
    const graphics::Texture texture{"res/container.jpg"};

    public:
    Texture() {
        vao.unbind();
        vbo.unbind();
        ebo.unbind();
    }

    void onUpdate() override {
        shader.enable();
        vao.bind();
        texture.bind();
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
};

}