namespace demos {

class Cube : public Game {
    static inline const std::string name = "Cube Demo";
    private:
    inline static const std::string vertexShaderSource =
R"(#version 330 core
layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}
)";

    inline static const std::string fragmentShaderSource =
R"(#version 330 core
out vec4 color;

void main() {
    color = vec4(1.0, 0.0, 1.0, 1.0);
}
)";

    inline static const std::array vertices {
        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f,  0.5f, 0.5f,
        -0.5f, 0.5f, 0.5f,

        -0.5f, -0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        0.5f,  0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f
    };

    inline static const std::array indices {
        0, 1, 2,
        2, 3, 0,
        // top
        3, 2, 6,
        6, 7, 3,
        // back
        7, 6, 5,
        5, 4, 7,
        // bottom
        4, 5, 1,
        1, 0, 4,
        // left
        4, 0, 3,
        3, 7, 4,
        // right
        1, 5, 6,
        6, 2, 1
    };

    const graphics::ShaderProgram shader {
        graphics::Shader { GL_VERTEX_SHADER, vertexShaderSource },
        graphics::Shader { GL_FRAGMENT_SHADER, fragmentShaderSource }
    };

    const graphics::VAO vao;
    const graphics::VBO vbo{vertices, 3};
    const graphics::EBO ebo{indices};

    glm::mat4 model = glm::mat4(1.f);

    public:
    Cube() {
        vao.unbind();
        vbo.unbind();
        ebo.unbind();

        shader.enable();

        glm::mat4 view = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -3.f));
        glm::mat4 projection = glm::perspective(glm::radians(45.f), 640.f / 480.f, 0.1f, 100.f);

        const auto viewLocation = shader.getUniformLocation("view");
        const auto projectionLocation = shader.getUniformLocation("projection");

        shader.uniformMatrix4fv(viewLocation, 1, GL_FALSE, glm::value_ptr(view));
        shader.uniformMatrix4fv(projectionLocation, 1, GL_FALSE, glm::value_ptr(projection));
    }

    void onUpdate() override {
        model  = glm::rotate(model, static_cast<float>(glfwGetTime()) * glm::radians(-0.005f), glm::vec3(0.1f, 0.05f, 0.f));

        shader.enable();
        const auto modelLocation = shader.getUniformLocation("model");
        shader.uniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(model));

        vao.bind();
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)0);
    }
};

}