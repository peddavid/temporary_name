class Game {
public:
virtual ~Game() { };
virtual void onUpdate() = 0;
};