## Quick Start

### Windows (MSVC)

**Test**

`test`

**Build**

`build`

_dependencies_
assimp x64: `cmake . -G"Visual Studio 15 2017 Win64"`

**Run**

`build\main`

### Linux (Clang/GCC)

Note: You can add a parameter `clang++` or `g++` to choose between the two (defaults to clang)

**Test**

`./test.sh`

**Build**

`./build.sh`

**Run**

`./build/main`
