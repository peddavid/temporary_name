#define CATCH_CONFIG_MAIN
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#ifdef CATCH
    #include <catch2/catch.hpp>
#else
    #include <doctest/doctest.h>
#endif

unsigned int factorial(unsigned int number) {
    return number == 0 ? 1 : factorial(number - 1) * number;
}

TEST_CASE("Factorials are computed") {
    CHECK(factorial(0) == 1);
    CHECK(factorial(1) == 1);
    CHECK(factorial(2) == 2);
    CHECK(factorial(3) == 6);
}
