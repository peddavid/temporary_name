#include <iostream>
#include <array>
#include <unordered_map>
#include <memory>
#include <numeric>
#include <string>
#include <vector>
#include <functional>

#pragma warning(push)
#pragma warning(disable: 4201 4127 6326)
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable: 6385 6386)
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#pragma warning(pop)

#include "src/fonts.cpp"
#include "src/graphics/graphics.cpp"
#include "src/graphics/fontRenderer.cpp"

#include "src/game.cpp"
#include "src/demos/text.cpp"
#include "src/demos/texture.cpp"
#include "src/demos/cube.cpp"
#include "src/demos/model.cpp"

#pragma warning(push)
#pragma warning(disable: 6387)
#include "fmt/format.h"
#pragma warning(pop)

struct DemoEntry {
    const std::string name;
    const std::function<std::unique_ptr<Game>()> create;
};

template<typename T>
DemoEntry getDemoEntry() {
    return { T::name, [](){ return std::make_unique<T>(); } };
}

int main() {
    using namespace graphics;
    using namespace fonts;
    using namespace std::string_literals;

    const WindowContext ctx;
    const Window window = ctx.make_window("Hello World", 640, 480);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    TextRenderer textRenderer;

    struct State {
        std::unique_ptr<Game> demo = nullptr;
        const std::array<DemoEntry, 4> demos {{
            getDemoEntry<demos::Text>(),
            getDemoEntry<demos::Texture>(),
            getDemoEntry<demos::Cube>(),
            getDemoEntry<demos::Model>(),
        }};
    } state;

    auto keyCallback = [&window, &state](int key, int, int action, int) {
        if (key >= GLFW_KEY_0 && key <= GLFW_KEY_9 && action == GLFW_PRESS) {
            if (state.demos.size() <= static_cast<std::size_t>(key - GLFW_KEY_0)) {
                return;
            }
            state.demo = state.demos[key - GLFW_KEY_0].create();
        } else if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            if (state.demo == nullptr) {
                window.close();
            } else {
                state.demo = nullptr;
            }
        }
    };
    window.setKeyCallback(keyCallback);

    while (!window.shouldClose()) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.f, 0.f, 0.f, 1.f);
        if (state.demo != nullptr) {
            state.demo->onUpdate();
        } else {
            int i { 0 };
            std::for_each(std::begin(state.demos), std::end(state.demos), [&textRenderer, &i](const auto& demo){
                textRenderer.render(fmt::format("{}: {}", i, demo.name), 20.f, 480.f - (40.f * (i + 1)), 1.f, glm::vec3(1.f, 0.0f, 1.f));
                i++;
            });
        }
        window.update();
    }
    return 0;
}
