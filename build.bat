@echo off

if not exist .\build mkdir .\build
pushd build

if "%1" == "32" (
    set arch="Win32"
) else (
    set arch="x64"
)

COPY /Y ..\lib\assimp\bin\%arch%\Release\assimp-vc140-mt.dll .

set CompileFlags= /W4 /analyze /Zi /EHsc /std:c++17
set Includes=-I../lib/glfw/include -I../lib/glad/include -I../lib/stb/include -I../lib/freetype2/include -I../lib/glm -I../lib/assimp/include -I../lib/fmt/include
set ExtLibs="../lib/glfw/builds/%arch%/glfw3.lib" "../lib/freetype2/objs/%arch%/Release Static/freetype.lib" "../lib/assimp/lib/%arch%/Release/assimp-vc140-mt.lib" "../lib/fmt/build/%arch%/fmt.lib"
set InternalLibs=opengl32.lib user32.lib kernel32.lib gdi32.lib shell32.lib vcruntime.lib msvcrt.lib

cl ../main.cpp ../lib/glad/src/glad.c %CompileFlags% %Includes% /link %ExtLibs% %InternalLibs%

popd
