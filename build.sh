#!/bin/bash

mkdir -p build

compiler=${1:-clang++}
${compiler} -std=c++17 main.cpp -o build/main -Wall -Wpedantic -Wextra
